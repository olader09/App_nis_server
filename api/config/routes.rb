Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mount ActionCable.server => '/cable'

  post :user_token, to: 'user_token#create'

  post :temp_users, to: 'temp_users#create'

  resource :user do
    post :update_basket, on: :member
  end

  resources :dishes

  post :superuser_token, to: 'superuser_token#create'

  resource :superuser do
    post :push_message_to_all, on: :member
    get :online_users, on: :member
    get :show_user, on: :member
  end

  resources :messages

  resources :chats
end
