class UsersController < APIBaseController
  before_action :load_user, except: :create
  authorize_resource except: :create
  before_action :auth_user, except: :create

  def show
    render json: @user.to_json(except: %i[password_digest push_token])
  end

  def update
    @user.update(update_user_params)
    if @user.errors.blank?
      render status: :ok
    else
      render json: @user.errors, status: :bad_request
    end
  end

  def create
    if User.where(phone_number: create_user_params['phone_number']).present?
      @user = User.find_by(phone_number: create_user_params['phone_number'])
      @user.generate_password(create_user_params['push_token'])
      render status: :ok
    else
      @user = User.create(create_user_params)
      @user.generate_password(create_user_params['push_token'])
      render status: :ok
    end
  end

  def destroy
    @user.delete
  end

  def update_basket
    @user.update(basket: params[:basket])
    if @user.errors.blank?
      render status: :ok
    else
      render json: @user.errors, status: :bad_request
    end
  end

  protected

  def load_user
    @user = current_user
  end

  def default_user_fields
    %i[name push_token basket]
  end

  def update_user_params
    params.required(:user).permit(
      *default_user_fields
    )
  end

  def create_user_params
    params.required(:user).permit(
      *default_user_fields, :phone_number
    )
  end
end
