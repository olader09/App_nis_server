class APIBaseController < ActionController::API
  before_action :init_redis
  include Knock::Authenticable

  def fcm_push_notification_to_all(data)
    fcm_client = FCM.new(Rails.application.secrets.api_fcm_token) # set your FCM_SERVER_KEY

    if data.type_message == 1
      body = data.content
    elsif data.type_message == 2
      body = 'Фотография'
    end
    options = { priority: 'high',
                data: { message: body },
                notification: { body: body,
                                title: 'У вас новое сообщение от ресторана!',
                                sound: 'default',
                                click_action: 'FLUTTER_NOTIFICATION_CLICK' } }
    registration_ids = User.where.not(push_token: 0).pluck(:push_token) + JSON.parse(@redis.get('temp_users_push_tokens'))

    registration_ids.uniq.each_slice(999) do |registration_id|
      response = fcm_client.send(registration_id, options)
      p response = JSON.parse(response[:body]).deep_transform_keys(&:to_sym)
      if response[:failure] > 0
        @invalid_pushes = []
        @temp_push_tokens = JSON.parse(@redis.get('temp_users_push_tokens'))
        results_pushes = response[:results]

        results_pushes.each_index do |index_result_push|
          @invalid_pushes << registration_ids[index_result_push] if results_pushes[index_result_push].include?(:error)
        end

        @invalid_pushes.each do |invalid_push|
          User.where(push_token: invalid_push).update_all(push_token: 0)
          @temp_push_tokens.delete(invalid_push)
        end
        @redis.set('temp_users_push_tokens', @temp_push_tokens)
      end
      return { "push_notification": { "success": response[:success], "failure": response[:failure] } }
    end
  end

  def current_ability
    @current_ability ||= if current_superuser.present?
                           Ability.new(current_superuser)
                         else
                           Ability.new(current_user)
                         end
  end

  protected

  def auth_user
    if current_superuser.present?
      authenticate_superuser
    else
      authenticate_user
      current_user.update(verify: true) if current_user.verify == false
    end
  end

  def init_redis
    @redis = Redis.new(host: 'redis', port: 6379, db: 15)
    @redis.set('temp_users_push_tokens', []) if @redis.get('temp_users_push_tokens').nil?
  end
end
