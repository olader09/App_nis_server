class TempUsersController < APIBaseController
  def create
    p JSON.parse(@redis.get('temp_users_push_tokens'))
    current_hash = JSON.parse(@redis.get('temp_users_push_tokens'))
    if current_hash.include?(params[:push_token])
      render status: :already_reported
    else
      @redis.set('temp_users_push_tokens', current_hash + [params[:push_token]])
      render status: :ok
    end
  end
end
