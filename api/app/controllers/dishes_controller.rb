class DishesController < APIBaseController
  authorize_resource except: %i[index show]

  before_action :auth_user, except: %i[index show]

  def index
    dishes = Dish.all.order(:zindex)
    if dishes.empty?
      render status: 204
    else
      render json: dishes
    end
  end

  def show
    @dish = Dish.find(params[:id])
    if @dish.errors.blank?
      render json: @dish, status: :ok
    else
      render json: @dish.errors, status: :bad_request
    end
  end

  def update
    @dish = Dish.find(params[:id])
    @dish.update(update_dish_params)
    if @dish.errors.blank?
      render status: :ok
    else
      render json: @dish.errors, status: :bad_request
    end
  end

  def create
    @dish = Dish.create(create_dish_params)
    if @dish.errors.blank?
      render status: :ok
    else
      render json: @dish.errors, status: :bad_request
    end
  end

  def destroy
    @dish = Dish.find(params[:id])
    if @dish.errors.blank?
      @dish.delete
      @dish.remove_picurl!
      @dish.save
      render status: :ok
    else
      render json: @dish.errors, status: :bad_request
    end
  end

  protected

  def default_dish_fields
    %i[category name subname picurl price caption1 caption2 caption3 zindex icon_index]
  end

  def update_dish_params
    params.required(:dish).permit(
      *default_dish_fields
    )
  end

  def create_dish_params
    params.required(:dish).permit(
      *default_dish_fields
    )
  end
end
