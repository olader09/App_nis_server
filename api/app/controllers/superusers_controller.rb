class SuperusersController < APIBaseController
  before_action :load_superuser, except: :create
  authorize_resource except: :create
  before_action :auth_user, except: :create

  def show
    render json: @superuser.to_json(except: [:password_digest])
  end

  def update
    @superuser.update(update_superuser_params)
    if @superuser.errors.blank?
      render status: :ok
    else
      render json: @superuser.errors, status: :bad_request
    end
  end

  def create
    @superuser = Superuser.create(create_superuser_params)
    if @superuser.errors.blank?
      render status: :ok
    else
      render json: @superuser.errors, status: :bad_request
    end
  end

  def destroy
    @superuser.delete
  end

  def online_users
    online_users = User.all.where(online: true)
    if online_users.empty?
      render status: :no_content
    else
      render json: online_users, except: %i[password_digest created_at], status: :ok
    end
  end

  def show_user
    user = User.find(params[:id])
    puts user
    if user.errors.blank?
      render json: user, except: [:password_digest], status: :ok
    else
      render status: :bad_request
    end
  end

  def push_message_to_all
    if params[:type_message].present? && params[:type_message] == 1
      all_users = User.all
      all_users.each do |user|
        @new_message = Message.create(content: params[:content], sender: current_superuser, chat_id: user.chat.id, type_message: 1)
        ActionCable.server.broadcast "room_channel_#{user.chat.id}", message: @new_message.content, type_message: 1, sender_type: 'Superuser', created_at: @new_message.created_at
      end
      render json: fcm_push_notification_to_all(@new_message), status: :ok
    elsif params[:type_message].present? && params[:picture].present? && params[:type_message] == 2
      all_users = User.all
      all_users.each do |user|
        @new_message = Message.create(picture: params[:picture], sender: current_superuser, chat_id: user.chat.id, type_message: 2)
        ActionCable.server.broadcast "room_channel_#{user.chat.id}", picture: @new_message.picture, type_message: 2, sender_type: 'Superuser', created_at: @new_message.created_at
      end
      render json: fcm_push_notification_to_all(@new_message), status: :ok
    else
      render json: { "error": 'need required params' }, status: :bad_request
    end
  end

  protected

  def load_superuser
    @superuser = current_superuser
  end

  def default_superuser_fields
    %i[btn1 btn2 btn3 push_token login]
  end

  def update_superuser_params
    params.required(:superuser).permit(
      *default_superuser_fields
    )
  end

  def create_superuser_params
    params.required(:superuser).permit(
      *default_superuser_fields, :password
    )
  end
end
