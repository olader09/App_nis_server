class Dish < ApplicationRecord
  after_create :init_zindex

  def init_zindex
    update_attributes(zindex: id + 1)
  end

  mount_uploader :picurl, DishUploader

  mount_base64_uploader :picurl, DishUploader
end
