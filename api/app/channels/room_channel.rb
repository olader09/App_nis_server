class RoomChannel < ApplicationCable::Channel
  MESSAGE_TYPES = [1, 2, 3].freeze

  def subscribed
    if current_user.superuser?
      current_user.update(room_id: chat_id) # При хаходе в чат записываем в бд текущий чат
      Message.where(chat_id: chat_id, sender_type: 'User', status: false).update_all(status: true) # Читаем сообщение Админом при входе в чат с юзером
    else
      current_user.update(online: true)
      ActionCable.server.broadcast room_id, message: 'Пользователь вошел в чат!', user_online: current_user.online, sender_type: 'System', type_message: 1
    end
    stream_from room_id
  end

  def unsubscribed
    if current_user.superuser?
      current_user.update(room_id: nil)
      Message.where(chat_id: chat_id, sender_type: 'User', status: false).update_all(status: true) # Читаем сообщение Админом при выходе из чата с юзером
    else
      current_user.update(online: false)
      ActionCable.server.broadcast room_id, message: 'Пользователь вышел из чата!', user_online: current_user.online, sender_type: 'System', type_message: 1
    end
  end

  def receive(data)
    type_message = data['type_message']
    picture = data['picture']
    content = data['message']
    return if type_message != 2 && content.blank?
    return unless MESSAGE_TYPES.include?(type_message)

    new_message = Message.new(content: content, sender: current_user, chat_id: chat_id, picture: picture, type_message: type_message)
    return unless new_message.save

    ActionCable.server.broadcast room_id, message: content, picture: new_message.picture, type_message: type_message, sender_type: new_message.sender_type, created_at: new_message.created_at

    notify_service = PushNotificationService.new(new_message, current_user, chat_id)
    notify_service.fcm_push_notification
  end

  protected

  def room_id
    "room_channel_#{chat_id}"
  end

  def chat_id
    if current_user.superuser?
      params['chat_id']
    else
      current_user.chat.id
    end
  end
end
