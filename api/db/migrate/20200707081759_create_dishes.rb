class CreateDishes < ActiveRecord::Migration[5.1]
  def change
    create_table :dishes do |t|
      t.string :category
      t.string :name, null: false
      t.string :subname
      t.string :picurl
      t.string :price, null: false
      t.string :caption1
      t.string :caption2
      t.string :caption3
      t.timestamps
    end
  end
end
