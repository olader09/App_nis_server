class AddIconIndexToDishes < ActiveRecord::Migration[5.1]
  def change
    add_column :dishes, :icon_index, :integer, null: false, default: 0
  end
end
