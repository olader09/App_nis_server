class AddDishesZindex < ActiveRecord::Migration[5.1]
  def change
    add_column :dishes, :zindex, :integer
  end
end
