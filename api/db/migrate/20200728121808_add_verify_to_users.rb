class AddVerifyToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :verify, :boolean, default: false
  end
end
